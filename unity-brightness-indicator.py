#!/usr/bin/env python3

# Copyright 2021 badito22@gmail.com
# 
#licensed under the GPLv2 license
#https://www.gnu.org/licenses/old-licenses/gpl-2.0.html
#based on the work of https://gist.github.com/thilinapiy/ad618059af25af8f1b8d

from gi.repository import AppIndicator3 as appindicator
from gi.repository import Gtk as gtk
import subprocess
import sys
import os.path


def max_brightness(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/BRIGHTNESS_MAX.sh"])

def mid_brightness(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/BRIGHTNESS_MID.sh"])

def min_brightness(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/BRIGHTNESS_MIN.sh"])

def call_control_center(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/OPEN_BRIGHTNESS_CONTROL.sh"])

def night_light_on(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/REDSHIFT_START_PROFILE.sh"])
    indicator.set_icon(os.path.dirname(os.path.realpath(__file__))+"/night_light_icon.svg")


def night_light_off(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/REDSHIFT_CLEANER.sh"])
    indicator.set_icon(os.path.dirname(os.path.realpath(__file__))+"/notification-display-brightness.svg")

def night_light_menu(self):
    subprocess.call([os.path.dirname(os.path.realpath(__file__)) + "/bin/REDSHIFT_MENU.sh"])

if __name__ == "__main__":
#    indicator = appindicator.Indicator.new("My Menu", "tray-new-im", appindicator.IndicatorCategory.APPLICATION_STATUS)
    indicator = appindicator.Indicator.new("Unity Brightness Indicator",  os.path.dirname(os.path.realpath(__file__)) + "/notification-display-brightness.svg" ,appindicator.IndicatorCategory.SYSTEM_SERVICES)
    indicator.set_status(appindicator.IndicatorStatus.ACTIVE)

    menu = gtk.Menu()

    maxbright = gtk.MenuItem("Maximum Brightness")
    maxbright.connect("activate",max_brightness)
    maxbright.show()
    menu.append(maxbright)

    midbright = gtk.MenuItem("Medium Brightness")
    midbright.connect("activate",mid_brightness)
    midbright.show()
    menu.append(midbright)

    minbright = gtk.MenuItem("Minimum Brightness")
    minbright.connect("activate",min_brightness)
    minbright.show()
    menu.append(minbright)

    brightcontrol = gtk.MenuItem("Open Brightness Settings")
    brightcontrol.connect("activate",call_control_center)
    brightcontrol.show()
    menu.append(brightcontrol)

    brightcontrol = gtk.MenuItem("Night Light: ON")
    brightcontrol.connect("activate",night_light_on)
    brightcontrol.show()
    menu.append(brightcontrol)

    brightcontrol = gtk.MenuItem("Night Light: OFF")
    brightcontrol.connect("activate",night_light_off)
    brightcontrol.show()
    menu.append(brightcontrol)

    brightcontrol = gtk.MenuItem("Open Night Light Settings")
    brightcontrol.connect("activate",night_light_menu)
    brightcontrol.show()
    menu.append(brightcontrol)

    menu.show()
    indicator.set_menu(menu)
    gtk.main()
