#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
REDSHIFT_PROFILE_PATH=`echo ${HOME}/.config/unity-brightness-indicator/nightlight_profiles`
REDSHIIFT_EXE=`/usr/bin/which redshift`
CURRENT_DIR=`pwd`


REDSHIFT_ERROR ()
{
	if [ $2 -ne 0 ]
	then
		zenity --error --width=300 --height=100 --text "$1"
		exit 1
	fi
}

REDSHIFT_GET_TIME () {
	REDSHIFT_MORNING=$( zenity  --width=500 --height=500 --scale --title "Daytime selection " --min-value=0 --max-value=23 --value=8 --text="Please select the hour of the day you wish to start the Daylight")
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
	REDSHIFT_NIGHT=$( zenity  --width=500 --height=500 --scale --title "Nighttime selection " --min-value=0 --max-value=23 --value=21 --text="Please select the hour of the day you wish to start the Night Light")
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
if [ $REDSHIFT_MORNING -lt 10 ]
then
	REDSHIFT_MORNING=`printf 0${REDSHIFT_MORNING}`
fi
if [ $REDSHIFT_NIGHT -lt 10 ]
then
	REDSHIFT_NIGHT=`printf 0${REDSHIFT_NIGHT}`
fi
REDSHIFT_MORNING=`printf ${REDSHIFT_MORNING}:00`
REDSHIFT_NIGHT=`printf ${REDSHIFT_NIGHT}:00`
}



REDHSIFT_COLOR_TEMP ()
{
	REDSHIFT_NIGHT_TEMP=$(zenity --scale --width=500 --height=500  --title="Night time Color Temperature" --min-value=3000 --max-value=6500 --value=${REDSHIFT_NIGHT_TEMP} --step=100 --text="3000 is more warm  and  6500 is less warm")
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
	if [ $REDSHIFT_CONTROL_TYPE == "manual" ]
	then
		REDSHIFT_DAY_TEMP=$REDSHIFT_NIGHT_TEMP
	else
	REDSHIFT_DAY_TEMP=$(zenity --scale --width=500 --height=500  --title="Daytime Color Temperature" --min-value=3000 --max-value=6500 --value=${REDSHIFT_DAY_TEMP} --step=100 --text="3000 is more warm  and  6500 is less warm")
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
	fi

}


REDSHIFT_CHECK_ACTIVE_PROFILE ()
{

		REDSHIFT_ACTIVE_PROFILE=$(zenity --question  --title="Question" --text "Do you wish this Profile to be active?\nOther active Profiles will be deactivated!" --no-wrap --ok-label "yes" --cancel-label "No")
		if [ $? -ne 0 ]
		then
			REDSHIFT_ACTIVE_PROFILE="No"
		else
			REDSHIFT_ACTIVE_PROFILE="yes"
			/bin/bash REDSHIFT_ACTIVE_PROFILE.sh $REDSHIFT_PROFILE_PATH $REDSHIFT_PROFILE_NAME 
			if [ $? -ne 0  ]
			then
			REDSHIFT_ERROR "ERROR: Profile activation failed." 1
			fi
		fi
}
#check for profile dir and right permisssion
if [  ! -d ${REDSHIFT_PROFILE_PATH} ]
then
REDSHIFT_ERROR "ERROR: $REDSHIFT_PROFILE_PATH does not exsist !" 1
fi

if [  ! -w ${REDSHIFT_PROFILE_PATH} ]
then
REDSHIFT_ERROR "ERROR: unable to write to $REDSHIFT_PROFILE_PATH ! Check permissions/ownership" 1
fi
cd $REDSHIFT_PROFILE_PATH
REDSHIFT_PROFILE_LIST="TRUE "
for i in *
do
        if [ -d $i ]
        then
                if [ "$i" != "default" ]
                then
                REDSHIFT_PROFILE_LIST="${REDSHIFT_PROFILE_LIST} ${i} FALSE "
                fi
        fi
done
cd $CURRENT_DIR
REDSHIFT_PROFILE_NAME=$( zenity --list --width=400 --height=500   --title "Select the Nigh Light Profile to edit"  --column Selection --column "Profiles" ${REDSHIFT_PROFILE_LIST%??????} --radiolist )
REDSHIFT_ERROR "ERROR: Profile Edit canceled! No changes will be made." $?
if [  ! -d $REDSHIFT_PROFILE_PATH/${REDSHIFT_PROFILE_NAME} ]
then
        REDSHIFT_ERROR "ERROR: PROFILE DOES NOT EXIST" 1
fi
cd ${REDSHIFT_PROFILE_PATH}/${REDSHIFT_PROFILE_NAME}
REDSHIFT_CONTROL_TYPE=$(cat control)
REDSHIFT_DAY_TEMP=$(cat color_day)
REDSHIFT_NIGHT_TEMP=$(cat color_night)
REDSHIFT_MORNING=$(cat time_day)
REDSHIFT_NIGHT=$(cat time_night)
cd $CURRENT_DIR
case $REDSHIFT_CONTROL_TYPE in
	manual)
REDSHIFT_CONTROL=$( zenity --list --width=400 --height=300   --title "How do you wish to control the Nigh Light?"  --column Selection --column "Control Type" TRUE "manual activation" FALSE "automatic timed"  FALSE "automatic using location services" --radiolist ) 
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
		;;
	autostart)
REDSHIFT_CONTROL=$( zenity --list --width=400 --height=300   --title "How do you wish to control the Nigh Light?"  --column Selection --column "Control Type" FALSE "manual activation" TRUE "automatic timed"  FALSE "automatic using location services" --radiolist ) 
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
		;;
	geo)
REDSHIFT_CONTROL=$( zenity --list --width=400 --height=300   --title "How do you wish to control the Nigh Light?"  --column Selection --column "Control Type" FALSE "manual activation" FALSE "automatic timed"  TRUE "automatic using location services" --radiolist ) 
REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." $?
		;;
	*) REDSHIFT_ERROR "ERROR: Invalid control type" 1
		;;
esac
case $REDSHIFT_CONTROL in 
	"manual activation")
		REDSHIFT_CONTROL_TYPE="manual"
		REDHSIFT_COLOR_TEMP 
		REDSHIFT_MORNING="03:59"
		REDSHIFT_NIGHT="04:00"
		REDSHIFT_LOC="manual"
		REDSHIFT_STARTUP=$(zenity --question  --title="Question" --text "Do you wish to start Night Light when you login?" --no-wrap --ok-label "yes" --cancel-label "No")
		if [ $? -ne 0 ]
		then
			REDSHIFT_STARTUP="No"
		else
			REDSHIFT_STARTUP="yes"
		fi
		;;
	"automatic timed")
		REDSHIFT_CONTROL_TYPE="autostart"
		REDHSIFT_COLOR_TEMP 
		REDSHIFT_GET_TIME
		REDSHIFT_LOC="manual"
		REDSHIFT_STARTUP="yes"
		;;
	"automatic using location services")
		REDSHIFT_CONTROL_TYPE="geo"
		zenity --info --title="Information about location based Night Light control" --width=400 --height=100 --text "Night Light might fail if your computer is not connected to the internet while using location services. Also make sure you have location services enabled in the Unity System Settings"
		REDHSIFT_COLOR_TEMP 
		REDSHIFT_LOC="geoclue2"
		REDSHIFT_STARTUP="yes"
		;;
	*) 
		REDSHIFT_ERROR "ERROR: Invalid control type" 1
		;;
esac


case $REDSHIFT_CONTROL_TYPE in
	manual)
		REDSHIFT_CONFIRM_TEXT=`printf 'Is the following Night Light configuration correct?\n\nProfile Name: '${REDSHIFT_PROFILE_NAME}'\nControl Type: Manual Activation\nStart at login: '${REDSHIFT_STARTUP}'\nNight Color Temperature: '${REDSHIFT_NIGHT_TEMP}'\n\n\n by pressing Yes you confirm that this profile should be saved'`
		;;
	autostart)
		REDSHIFT_CONFIRM_TEXT=`printf 'Is the following Night Light configuration correct?\n\nProfile Name: '${REDSHIFT_PROFILE_NAME}'\nControl Type: Automatic Timed\nStart at login: '${REDSHIFT_STARTUP}'\nNight Color Temperature: '${REDSHIFT_NIGHT_TEMP}'\nDay Color Temperature: '${REDSHIFT_DAY_TEMP}'\nDaylight Start time: '${REDSHIFT_MORNING}'\nNight Light Start time: '${REDSHIFT_NIGHT}'\n\n\n by pressing Yes you confirm that this profile should be saved'`
		;;
	geo)
		REDSHIFT_CONFIRM_TEXT=`printf 'Is the following Night Light configuration correct?\n\nProfile Name: '${REDSHIFT_PROFILE_NAME}'\nControl Type: Automatic using location services\nStart at login: '${REDSHIFT_STARTUP}'\nNight Color Temperature: '${REDSHIFT_NIGHT_TEMP}'\nDay Color Temperature: '${REDSHIFT_DAY_TEMP}'\n\n\n by pressing Yes you confirm that this profile should be saved'`
		;;
	*)
		REDSHIFT_ERROR "ERROR: Invalid control type" 1
		;;

esac

REDSHIFT_PROFILE_CHANGE= $( zenity --question  --title="Confirm Profile Changes" --text "${REDSHIFT_CONFIRM_TEXT}" --no-wrap --ok-label "Yes" --cancel-label "No" )
if [ $? -ne 0  ]
then
	REDSHIFT_ERROR "ERROR: Creation canceled! No changes will be made." 1
fi

/bin/bash REDSHIFT_PROFILE_GENERATOR.sh $REDSHIFT_PROFILE_PATH $REDSHIFT_PROFILE_NAME $REDSHIFT_CONTROL_TYPE $REDSHIFT_DAY_TEMP $REDSHIFT_NIGHT_TEMP $REDSHIFT_MORNING $REDSHIFT_NIGHT $REDSHIFT_LOC $REDSHIFT_STARTUP
if [ $? -ne 0  ]
then
	REDSHIFT_ERROR "ERROR: Profile creation failed." 1
fi
/bin/bash REDSHIFT_COMMAND_GENERATOR.sh $REDSHIFT_PROFILE_PATH $REDSHIFT_PROFILE_NAME
if [ $? -ne 0  ]
then
	REDSHIFT_ERROR "ERROR: Command creation failed." 1
fi
REDSHIFT_CHECK_ACTIVE_PROFILE
/bin/bash REDSHIFT_START_PROFILE.sh
