#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
#parameters: none
REDSHIFT_PROFILE_PATH=`echo ${HOME}/.config/unity-brightness-indicator/nightlight_profiles`
test -f $REDSHIFT_PROFILE_PATH/active
if [ $? -ne 0 ]
then
zenity --error --title="Night Light Error" --width=300 --height=100 --text "Error: no active Profile found. Please check the Night Light Settings! "
exit 1
fi
REDSHIFT_PROFILE_NAME=`cat $REDSHIFT_PROFILE_PATH/active`
test -d $REDSHIFT_PROFILE_PATH/$REDSHIFT_PROFILE_NAME 
if [ $? -ne 0 ]
then
zenity --error --title="Night Light Error" --width=300 --height=100 --text "Error: no active Profile found. Please check the Night Light Settings! "
exit 1
fi
REDSHIFT_STARTUP=`cat $REDSHIFT_PROFILE_PATH/$REDSHIFT_PROFILE_NAME/startup`
case $REDSHIFT_STARTUP in
No)
	exit 0
	;;
yes)
	/bin/bash /opt/unity-brightness-indicator/bin/REDSHIFT_START_PROFILE.sh
	exit 0
	;;
*)
	zenity --error --title="Night Light Error" --width=300 --height=100 --text "Error: autostart setting invalid! Please edit the profile ${REDSHIFT_PROFILE_NAME} in the Night Light Settings!"
	exit 1
	;;
esac
