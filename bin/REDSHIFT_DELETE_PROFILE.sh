#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
REDSHIFT_PROFILE_PATH=`echo ${HOME}/.config/unity-brightness-indicator/nightlight_profiles`
REDSHIIFT_EXE=`/usr/bin/which redshift`
CURRENT_DIR=`pwd`

REDSHIFT_ERROR ()
{
	if [ $2 -ne 0 ]
	then
		zenity --error --width=300 --height=100 --text "$1"
		exit 1
	fi
}

REDSHIFT_DELETE ()
{
 REDSHIFT_ACTIVE_PROFILE=$(cat $REDSHIFT_PROFILE_PATH/active)
 if [ ${REDSHIFT_ACTIVE_PROFILE} == ${REDSHIFT_PROFILE_NAME} ]
 then
	 zenity --info --title="Night Light Profile Deletion" --width=400 --height=200 --text "Profile ${REDSHIFT_PROFILE_NAME} is  active! You need to switch to a different Profile first before you can delete it.\n\nYou will be redirected to the Night Light Profile Switcher if you press OK"
	 /bin/bash REDSHIFT_SWITCH_ACTIVE_PROFILE.sh
	 REDSHIFT_ERROR "ERROR: Deletion canceled! Profile deactivation failed!" $?
 fi
 #moving to trash instead of rm, just in case
/usr/bin/gio trash  $REDSHIFT_PROFILE_PATH/$REDSHIFT_PROFILE_NAME
REDSHIFT_ERROR "ERROR: Deletion failed! Error was returned" $?
zenity --info --title="Night Light Profile Deletion" --width=400 --height=100 --text "Profile ${REDSHIFT_PROFILE_NAME} has been moved to the Trash bin."
}

#check for profile dir and right permisssion

if [  ! -w ${REDSHIFT_PROFILE_PATH} ]
then
REDSHIFT_ERROR "ERROR: unable to write to $REDSHIFT_PROFILE_PATH ! Check permissions/ownership" 1
fi
cd ${REDSHIFT_PROFILE_PATH}
REDSHIFT_PROFILE_LIST="TRUE "
for i in *
do
	if [ -d $i ]
	then
		if [ "$i" != "default" ]
		then
		REDSHIFT_PROFILE_LIST="${REDSHIFT_PROFILE_LIST} ${i} FALSE "
		fi
	fi
done
cd $CURRENT_DIR
REDSHIFT_PROFILE_NAME=$( zenity --list --width=400 --height=500   --title "Select the Nigh Light Profile to delete"  --column Selection --column "Profiles" ${REDSHIFT_PROFILE_LIST%??????} --radiolist )
REDSHIFT_ERROR "ERROR: Deletion canceled! No changes will be made." $?
if [  ! -d $REDSHIFT_PROFILE_PATH/${REDSHIFT_PROFILE_NAME} ]
then
	REDSHIFT_ERROR "ERROR: PROFILE DOES NOT EXIST" 1
fi
  REDSHIFT_CHECK_DELETE=$(zenity --question  --title="Confirm Deletion" --text "Do you wish to delete the Night Light Profile $REDSHIFT_PROFILE_NAME?" --no-wrap --ok-label "Yes" --cancel-label "No")
if [ $? -ne 0 ]
then
REDSHIFT_CHECK_DELETE="no"
else
REDSHIFT_CHECK_DELETE="yes"
fi

case $REDSHIFT_CHECK_DELETE in
	yes) REDSHIFT_DELETE
		;;
	no) REDSHIFT_ERROR "ERROR: Deletion canceled! No changes will be made." 1
		;;
esac
exit 0
