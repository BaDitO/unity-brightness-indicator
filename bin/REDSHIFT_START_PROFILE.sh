#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
#parameters: none
REDSHIFT_PROFILE_PATH=`echo ${HOME}/.config/unity-brightness-indicator/nightlight_profiles`
test -f $REDSHIFT_PROFILE_PATH/active
if [ $? -ne 0 ]
then
zenity --error --title="Night Light Error" --width=300 --height=100 --text "Error: no active Profile found. Please check the Night Light Settings! "
exit 1
fi
REDSHIFT_PROFILE_NAME=`cat $REDSHIFT_PROFILE_PATH/active`
test -d $REDSHIFT_PROFILE_PATH/$REDSHIFT_PROFILE_NAME 
if [ $? -ne 0 ]
then
zenity --error --title="Night Light Error" --width=300 --height=100 --text "Error: no active Profile found. Please check the Night Light Settings! "
exit 1
fi
/bin/bash /opt/unity-brightness-indicator/bin/REDSHIFT_CLEANER.sh
sleep 2 
/bin/bash $REDSHIFT_PROFILE_PATH/$REDSHIFT_PROFILE_NAME/nightlight_command.sh & 
/usr/bin/notify-send -i /opt/unity-brightness-indicator/night_light_icon.svg  "Night Light: ON"

exit 0
