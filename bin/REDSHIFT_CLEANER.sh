#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
#parameters: 1=REDSHIFT_PROFILE_PATH 2=REDSHIFT_PROFILE_NAME
REDSHIFT_PROFILE_PATH=$1
REDSHIFT_PROFILE_NAME=$2
CURRENT_DIR=`pwd`
for i in $( ps -ef | grep '${REDSHIFT_PROFILE_PATH}/${REDSHIFT_PROFILE_NAME}' | grep -v grep | awk '{ print $2 }')
do
kill $i
done
#cleaning up other possible redshift jobs
ps -ef | grep -v grep | grep redshift > /dev/null 
if [ $? -eq 0 ]
then
	for i in $(ps -ef | grep -v grep | grep redshift  | awk '{ print $2 }')
	do
		kill $i
	done
fi
exit 0
