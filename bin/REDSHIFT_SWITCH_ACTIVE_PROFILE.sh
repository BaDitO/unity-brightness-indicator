#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
REDSHIFT_PROFILE_PATH=`echo ${HOME}/.config/unity-brightness-indicator/nightlight_profiles`
REDSHIIFT_EXE=`/usr/bin/which redshift`
CURRENT_DIR=`pwd`

REDSHIFT_ERROR ()
{
	if [ $2 -ne 0 ]
	then
		zenity --error --width=300 --height=100 --text "$1"
		exit 1
	fi
}

REDSHIFT_DELETE ()
{
echo blub
}


#check for profile dir and right permisssion

if [  ! -w ${REDSHIFT_PROFILE_PATH} ]
then
REDSHIFT_ERROR "ERROR: unable to write to $REDSHIFT_PROFILE_PATH ! Check permissions/ownership" 1
fi
cd ${REDSHIFT_PROFILE_PATH}
REDSHIFT_PROFILE_LIST="TRUE "
for i in *
do
	if [ -d $i ]
	then
		REDSHIFT_PROFILE_LIST="${REDSHIFT_PROFILE_LIST} ${i} FALSE "
	fi
done
cd $CURRENT_DIR
REDSHIFT_PROFILE_NAME=$( zenity --list --width=400 --height=500   --title "Select the Nigh Light Profile to activate"  --column Selection --column "Profiles" ${REDSHIFT_PROFILE_LIST%??????} --radiolist )
REDSHIFT_ERROR "ERROR: Activation canceled! No changes will be made." $?
if [  ! -d $REDSHIFT_PROFILE_PATH/${REDSHIFT_PROFILE_NAME} ]
then
	REDSHIFT_ERROR "ERROR: PROFILE DOES NOT EXIST" 1
fi
/bin/bash REDSHIFT_ACTIVE_PROFILE.sh $REDSHIFT_PROFILE_PATH $REDSHIFT_PROFILE_NAME
if [ $? -ne 0  ]
then
REDSHIFT_ERROR "ERROR: Profile activation failed." 1
fi
zenity --info --title="Night Light Profile Activation" --width=400 --height=100 --text "Profile ${REDSHIFT_PROFILE_NAME} is now active!"
exit 0
