#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
#parameters: 1=REDSHIFT_PROFILE_PATH 2=REDSHIFT_PROFILE_NAME 
REDSHIFT_PROFILE_PATH=$1
REDSHIFT_PROFILE_NAME=$2
CURRENT_DIR=`pwd`

test -f $REDSHIFT_PROFILE_PATH/active
if [ $? -eq 0 ]
then
	OLD_ACTIVE_PROFILE=`cat $REDSHIFT_PROFILE_PATH/active`
	/bin/bash REDSHIFT_CLEANER.sh $REDSHIFT_PROFILE_PATH $OLD_ACTIVE_PROFILE 
fi
echo $REDSHIFT_PROFILE_NAME > $REDSHIFT_PROFILE_PATH/active
exit 0
