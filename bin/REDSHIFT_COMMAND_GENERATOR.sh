#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
#parameters: 1=REDSHIFT_PROFILE_PATH 2=REDSHIFT_PROFILE_NAME 
REDSHIFT_PROFILE_PATH=$1
REDSHIFT_PROFILE_NAME=$2
CURRENT_DIR=`pwd`
REDSHIIFT_EXE=`/usr/bin/which redshift`
test -d $REDSHIFT_PROFILE_PATH
if [ $? -ne 0 ]
then
        exit 1
fi
cd $REDSHIFT_PROFILE_PATH
test -d $REDSHIFT_PROFILE_NAME
if [ $? -ne 0 ]
then
exit 1
fi
cd $REDSHIFT_PROFILE_NAME
set -e 
REDSHIFT_CONTROL_TYPE=`cat control`
REDSHIFT_DAY_TEMP=`cat color_day`
REDSHIFT_NIGHT_TEMP=`cat color_night`
REDSHIFT_MORNING=`cat time_day`
REDSHIFT_NIGHT=`cat time_night`
REDSHIFT_LOC=`cat location`
REDSHIFT_STARTUP=`cat startup`
echo "[redshift]" > redshift.conf
echo "temp-day="${REDSHIFT_DAY_TEMP} >> redshift.conf
echo "temp-night="${REDSHIFT_NIGHT_TEMP} >> redshift.conf
echo "location-provider="${REDSHIFT_LOC} >> redshift.conf
echo "adjustment-method=randr"  >> redshift.conf
if [ $REDSHIFT_CONTROL_TYPE!="geo" ]
then
echo "dawn-time="${REDSHIFT_MORNING} >> redshift.conf
echo "dusk-time="${REDSHIFT_NIGHT} >> redshift.conf
fi
echo $REDSHIIFT_EXE" -c " $REDSHIFT_PROFILE_PATH/$REDSHIFT_PROFILE_NAME/redshift.conf > nightlight_command.sh
chmod +x nightlight_command.sh
cd $CURRENT_DIR
exit 0
