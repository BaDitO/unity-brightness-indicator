#!/bin/bash
# Author: badito22@gmail.com
# copyright 2021
# Redshift frontend wrapper for the unity-brightness-idicator tool
# the following code is under GPLv2
CURRENT_DIR=`pwd`
EXEC_DIR=`dirname "$0"`
cd $EXEC_DIR
while true 
	do
		MENUCHOICE=$(zenity --width=500 --height=300 --list --column "Options:" --window-icon=/opt/unity-brightness-indicator/notification-display-brightness.svg --cancel-label="Quit" --ok-label="Select" --title="Night Light Options" "Create New Night Light Profile" "Edit Night Light Profile" "Delete Night Light Profile" "Change Active Profile" )

		case  $MENUCHOICE in
			"Create New Night Light Profile")
			       	/bin/bash REDSHIFT_CREATE_PROFILE.sh
				;;
			"Edit Night Light Profile") 
				/bin/bash REDSHIFT_EDIT_PROFILE.sh
				;;
			"Delete Night Light Profile")
				/bin/bash REDSHIFT_DELETE_PROFILE.sh
			       ;;
			"Change Active Profile")
			       /bin/bash REDSHIFT_SWITCH_ACTIVE_PROFILE.sh
			       ;;
			 *) exit 1
				;;
		esac
	done

cd $CURRENT_DIR

