unity-brightness-indicator tool
a appindicator for the ubuntu unity desktop which allows your to control the brightness and night light / redshift

author: badito22@gmail.com

license: gplv2, see the license file inside for further information

depends on unity-control-center (>= 15.04.0+19.10.20190921-0ubuntu3), gir1.2-appindicator3-0.1 (>= 12.10.1+20.04.20200408.1-0ubuntu1), xbacklight (>= 1.2.1-1build2), redshift (>= 1.12-2)


for prepacked Downloads look at the "PKG" directory in this project. 

Tested Platforms: ubuntu 20.04, ubuntu 21.10

How to install it: 


Install:
--------------

download: https://gitlab.com/BaDitO/unity-brightness-indicator/-/raw/main/PKG/unity-brightness-indicator_1.0.2_amd64.deb

cd ~/Downloads # or whatever download folder you use

sudo apt install ./unity-brightness-indicator_1.0.2_amd64.deb


uninstall:
-----------
sudo apt remove unity-brightness-indicator



